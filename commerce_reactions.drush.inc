<?php

/**
 * @file
 * Commerce Reactions module integration with Drush 8 and earlier.
 */

use Drupal\commerce_reactions\Event\CommerceReactionsEvents;
use Drupal\commerce_reactions\Event\DrushInitEvent;

/**
 * Implements hook_drush_init().
 */
function commerce_reactions_drush_init() {
  if (\Drupal::hasService('event_dispatcher')) {
    $event = new DrushInitEvent();
    \Drupal::service('event_dispatcher')->dispatch(CommerceReactionsEvents::DRUSH_INIT, $event);
  }
}
