CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Commerce Reactions module allows site administrators to define conditionally
executed actions based on occurring Drupal Commerce events.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/commerce_reactions

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/commerce_reactions


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

* Drupal Commerce - https://www.drupal.org/project/commerce
* Token - https://www.drupal.org/project/token
* Entity API - https://www.drupal.org/project/entity


INSTALLATION
------------

* Install the Commerce Reactions module as you would normally install a
  contributed Drupal module.
  Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Commerce > Configuration > Store > Reactions
   and "Add reaction". Save.


MAINTAINERS
-----------

Sponsors:

* Fundación UNICEF Comité Español - https://www.unicef.es

Contact:

* Developed and maintained by Cambrico - http://cambrico.net

* Get in touch with us for customizations and consultancy:
  http://cambrico.net/contact

Current maintainers:

* Pedro Cambra (pcambra) - http://drupal.org/user/122101
* Manuel Egío (facine) - http://drupal.org/user/1169056
