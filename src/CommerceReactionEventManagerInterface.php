<?php

namespace Drupal\commerce_reactions;

/**
 * Defines an interface for commerce reactions event manager classes.
 */
interface CommerceReactionEventManagerInterface {

  /**
   * Gets a specific event.
   *
   * @param string $event_id
   *   An event ID.
   * @param bool $exception_on_invalid
   *   (optional) If TRUE, an invalid event ID will throw an exception.
   *
   * @return mixed
   *   A event definition, or NULL if the event ID is invalid and
   *   $exception_on_invalid is FALSE.
   *
   * @throws \Drupal\commerce_reactions\Exception\EventNotFoundException
   *   Thrown if $event_id is invalid and $exception_on_invalid is TRUE.
   */
  public function getEvent($event_id, $exception_on_invalid = TRUE);

  /**
   * Gets a specific event's entity contexts.
   *
   * @param string $event_id
   *   An event ID.
   * @param bool $exception_on_invalid
   *   (optional) If TRUE, an invalid event ID will throw an exception.
   *
   * @return mixed
   *   A event's entity contexts definition, or NULL if the event ID is invalid
   *   and $exception_on_invalid is FALSE.
   *
   * @throws \Drupal\commerce_reactions\Exception\EventNotFoundException
   *   Thrown if $event_id is invalid and $exception_on_invalid is TRUE.
   */
  public function getEventEntityContexts($event_id, $exception_on_invalid = TRUE);

  /**
   * Gets all event groups.
   *
   * @return mixed[]
   *   An array of event groups (empty array if no groups were found).
   */
  public function getEvents();

  /**
   * Gets a specific event group.
   *
   * @param string $group_id
   *   An event group ID.
   * @param bool $exception_on_invalid
   *   (optional) If TRUE, an invalid event group ID will throw an exception.
   *
   * @return mixed
   *   A event group definition, or NULL if the event group ID is invalid and
   *   $exception_on_invalid is FALSE.
   *
   * @throws \Drupal\commerce_reactions\Exception\EventGroupNotFoundException
   *   Thrown if $group_id is invalid and $exception_on_invalid is TRUE.
   */
  public function getEventsGroup($group_id, $exception_on_invalid = TRUE);

  /**
   * Prepare #options array for event list.
   *
   * @return array
   *   The prepared array of events.
   */
  public function getEventOptions();

  /**
   * Clears static and persistent event group definition caches.
   */
  public function clearCachedDefinitions();

}
