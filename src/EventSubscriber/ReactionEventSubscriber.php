<?php

namespace Drupal\commerce_reactions\EventSubscriber;

use Drupal\commerce_reactions\CommerceReactionEventManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_reactions\Event\CommerceReactionsEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event as SymfonyComponentEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\EventDispatcher\Event as SymfonyContractsEvent;

/**
 * Map symfony events used by commerce reaction entities.
 */
class ReactionEventSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The commerce reaction event manager.
   *
   * @var \Drupal\commerce_reactions\CommerceReactionEventManagerInterface
   */
  protected $eventManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Events to subscribe if container is not available.
   *
   * @var array
   */
  private static $staticEvents = [
    KernelEvents::CONTROLLER => ['registerDynamicEvents', 100],
    KernelEvents::REQUEST => ['registerDynamicEvents', 100],
    KernelEvents::TERMINATE => ['registerDynamicEvents', 100],
    KernelEvents::VIEW => ['registerDynamicEvents', 100],
    CommerceReactionsEvents::DRUSH_INIT => ['registerDynamicEvents', 100],
  ];

  /**
   * Construct a new ReactionEventSubscriber object.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_reactions\CommerceReactionEventManagerInterface $event_manager
   *   The commerce reaction event manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, EntityTypeManager $entity_type_manager, CommerceReactionEventManagerInterface $event_manager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->eventManager = $event_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Register this listener for every event that is used by a reaction.
    $events = [];
    $callback = ['onReactionEvent', 100];

    // If there is no entity type manager service there is nothing we can do
    // here. This static method could be called early when the container is
    // built, so the service might not always be available.
    if (\Drupal::hasService('entity_type.manager') && \Drupal::entityTypeManager()->hasDefinition('commerce_reaction')) {
      $reaction_events = \Drupal::entityTypeManager()->getStorage('commerce_reaction')->getRegisteredEvents();
    }
    else {
      return self::$staticEvents;
    }

    foreach ($reaction_events as $event_name) {
      $events[$event_name][] = $callback;
    }

    return $events;
  }

  /**
   * Rebuilds container when dynamic event subscribers are not registered.
   *
   * @param object $event
   *   The event to process.
   *   In Drupal 9 this will be a \Symfony\Component\EventDispatcher\Event,
   *   In Drupal 10 this will be a \Symfony\Contracts\EventDispatcher\Event.
   *
   * @param string $event_name
   *   The event name.
   */
  public function registerDynamicEvents(object $event, $event_name) {
    // @todo The 'object' type hint should be replaced with the appropriate class once Symfony 4 is no longer supported, and the assert() should be removed.
    assert(
      $event instanceof SymfonyComponentEvent ||
      $event instanceof SymfonyContractsEvent
    );

    foreach (self::$staticEvents as $old_event_name => $method) {
      $listener = [$this, $method[0]];
      $this->eventDispatcher->removeListener($old_event_name, $listener);
    }
    $this->eventDispatcher->addSubscriber($this);
    $this->moduleHandler->reload();
  }

  /**
   * Reacts on the given event and invokes configured commerce reactions.
   *
   * @param object $event
   *   The event to process.
   *   In Drupal 9 this will be a \Symfony\Component\EventDispatcher\Event,
   *   In Drupal 10 this will be a \Symfony\Contracts\EventDispatcher\Event.
   * @param string $event_name
   *   The event name.
   */
  public function onReactionEvent(object $event, $event_name) {
    // @todo The 'object' type hint should be replaced with the appropriate class once Symfony 4 is no longer supported, and the assert() should be removed.
    assert(
      $event instanceof SymfonyComponentEvent ||
      $event instanceof SymfonyContractsEvent
    );

    try {
      $event_definition = $this->eventManager->getEvent($event_name);

      $contexts = [];
      foreach ($event_definition['contexts'] as $context_name => $context) {
        $contexts[$context_name] = call_user_func([$event, $context['getter']]);

        // Load the user if it isn't.
        if (!empty($context['entity_id']) &&
          $context['entity_id'] == 'user' &&
          $contexts[$context_name] instanceof AccountInterface) {

          $contexts[$context_name] = $this->entityTypeManager->getStorage('user')->load($contexts[$context_name]->id());
        }
      }

      /** @var \Drupal\commerce_reactions\Entity\Reaction[] $reactions */
      $reactions = $this->entityTypeManager
        ->getStorage('commerce_reaction')
        ->loadByProperties(['event' => $event_name]);

      foreach ($reactions as $reaction) {
        if ($reaction->applies($contexts)) {
          $reaction->getPlugin()->execute($contexts);
        }
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('commerce_reactions', $exception);
    }
  }

}
