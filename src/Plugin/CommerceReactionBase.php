<?php

namespace Drupal\commerce_reactions\Plugin;

use Drupal\commerce_reactions\CommerceReactionThemeManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\Definition\DependentPluginDefinitionTrait;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Utility\Token;
use Drupal\token\TreeBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the base class for commerce reactions.
 */
abstract class CommerceReactionBase extends PluginBase implements CommerceReactionInterface {

  use DependentPluginDefinitionTrait;

  /**
   * The ID of the parent config entity.
   *
   * Not available while the plugin is being configured.
   *
   * @var string
   */
  protected $entityId;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The commerce reactions theme manager.
   *
   * @var \Drupal\commerce_reactions\CommerceReactionThemeManagerInterface
   */
  protected $themeManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $tokenManager;

  /**
   * An array of objects.
   *
   * @var array
   */
  protected $tokenData = [];

  /**
   * An array of settings and flags to control the token replacement process.
   *
   * @var array
   */
  protected $tokenOptions = ['clear' => TRUE];

  /**
   * The tree builder.
   *
   * @var \Drupal\token\TreeBuilderInterface
   */
  protected $treeBuilder;

  /**
   * Constructs a new CommerceReactionBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\commerce_reactions\CommerceReactionThemeManagerInterface $theme_manager
   *   The commerce reactions theme manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\token\TreeBuilderInterface $tree_builder
   *   The tree builder.
   * @param \Drupal\Core\Utility\Token $token_manager
   *   The token service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger_factory, ModuleHandlerInterface $module_handler, CommerceReactionThemeManagerInterface $theme_manager, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager, TreeBuilderInterface $tree_builder, Token $token_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->themeManager = $theme_manager;
    $this->languageManager = $language_manager;
    $this->loggerFactory = $logger_factory;
    $this->moduleHandler = $module_handler;
    $this->tokenManager = $token_manager;
    $this->treeBuilder = $tree_builder;

    if (array_key_exists('_entity_id', $configuration)) {
      $this->entityId = $configuration['_entity_id'];
      unset($configuration['_entity_id']);
    }
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('module_handler'),
      $container->get('commerce_reactions.theme_manager'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('token.tree_builder'),
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $configuration = [];

      $values = $form_state->getValue($form['#parents']);
      foreach ($this->defaultConfiguration() as $key => $value) {
        if (isset($values[$key])) {
          $configuration[$key] = $values[$key];
        }
      }

      $this->setConfiguration($configuration);
      $this->setConfigDependencies($this->calculateDependencies());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getReactionId() {
    return $this->entityId;
  }

  /**
   * {@inheritdoc}
   */
  public function setReactionId($reaction_id) {
    $this->entityId = $reaction_id;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(array $context = []) {
    call_user_func([$this, 'doExecute'], $context);
  }

  /**
   * Send an email.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   */
  protected function doExecute(array $contexts) {
    $this->tokenData = [];
    foreach ($contexts as $item) {
      if ($item instanceof EntityInterface) {
        $this->tokenData[$item->getEntityTypeId()] = $item;
      }
    }
  }

  /**
   * Returns commerce reactions logger.
   *
   * @param string $channel
   *   The logger channel. Defaults to 'commerce_reactions'.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   Webform logger
   */
  protected function getLogger($channel = 'commerce_reactions') {
    return $this->loggerFactory->get($channel);
  }

  /**
   * Returns the language as associative array.
   *
   * @return array
   *   An associative array containing language  name.
   */
  protected function getLanguageOptions() {
    $language_options = [];
    foreach ($this->languageManager->getLanguages() as $langcode => $language) {
      $language_options[$langcode] = $language->getName();
    }

    return $language_options;
  }

}
