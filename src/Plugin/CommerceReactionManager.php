<?php

namespace Drupal\commerce_reactions\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\CategorizingPluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages commerce reaction plugins.
 */
class CommerceReactionManager extends DefaultPluginManager {

  use CategorizingPluginManagerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CommerceReactionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct('Plugin/CommerceReaction', $namespaces, $module_handler, 'Drupal\commerce_reactions\Plugin\CommerceReactionInterface', 'Drupal\commerce_reactions\Annotation\CommerceReaction');

    $this->entityTypeManager = $entity_type_manager;

    $this->alterInfo('commerce_reactions_reaction_info');
    $this->setCacheBackend($cache_backend, 'commerce_reactions_reaction_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    foreach (['id', 'label'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new PluginException(sprintf('The commerce reaction %s must define the %s property.', $plugin_id, $required_property));
      }
    }

    foreach ($definition['entity_types'] as $entity_type_id) {
      if (!$this->entityTypeManager->getDefinition($entity_type_id)) {
        throw new PluginException(sprintf('The commerce reaction "%s" must specify a valid entity type, "%s" given.', $plugin_id, $entity_type_id));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFilteredDefinitions(array $entity_type_ids) {
    $definitions = $this->getDefinitions();
    foreach ($definitions as $plugin_id => $definition) {
      // Filter by entity type.
      if (!empty($definition['entity_types'])) {
        $exists_type = FALSE;
        foreach ($definition['entity_types'] as $entity_type_id) {
          if (in_array($entity_type_id, $entity_type_ids)) {
            $exists_type = TRUE;
            continue;
          }
        }
        if (!$exists_type) {
          unset($definitions[$plugin_id]);
        }
      }
    }

    // Sort by weigh and label.
    uasort($definitions, function ($a, $b) {
      return strnatcasecmp($a['label'], $b['label']);
    });

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilteredAndGroupedOptions(array $entity_type_ids) {
    $definitions = $this->getFilteredDefinitions($entity_type_ids);

    $options = [];
    foreach ($definitions as $plugin_id => $definition) {
      $options[$definition['category']->__toString()][$plugin_id] = $definition['label'];
    }
    ksort($options);

    return $options;
  }

}
