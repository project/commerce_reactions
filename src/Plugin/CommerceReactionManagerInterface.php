<?php

namespace Drupal\commerce_reactions\Plugin;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;

/**
 * Defines the interface for commerce_reaction plugin managers.
 */
interface CommerceReactionManagerInterface extends CategorizingPluginManagerInterface {

  /**
   * Gets the filtered plugin definitions.
   *
   * @param array $entity_type_ids
   *   The entity type IDs. For example: ['commerce_order'] to get
   *   only commerce reactions that evaluate orders.
   *
   * @return array
   *   The filtered plugin definitions.
   */
  public function getFilteredDefinitions(array $entity_type_ids);

  /**
   * Prepare #options array for plugin list.
   *
   * @param array $entity_type_ids
   *   The entity type IDs. For example: ['commerce_order'] to get only commerce
   *   reactions that evaluate orders.
   *
   * @return array
   *   The prepared array of plugins.
   */
  public function getFilteredAndGroupedOptions(array $entity_type_ids);

}
