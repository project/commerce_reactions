<?php

namespace Drupal\commerce_reactions\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the base interface for commerce reactions.
 */
interface CommerceReactionInterface extends ConfigurableInterface, PluginFormInterface, PluginInspectionInterface, DependentPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Gets the config dependencies of this plugin definition.
   *
   * @return array
   *   An array of config dependencies.
   */
  public function getConfigDependencies();

  /**
   * Gets the commerce reaction label.
   *
   * @return string
   *   The commerce reaction label.
   */
  public function getLabel();

  /**
   * Returns the unique ID representing the commerce reaction.
   *
   * @return string
   *   The commerce reaction ID.
   */
  public function getReactionId();

  /**
   * Sets the ID for this commerce reaction.
   *
   * @param string $reaction_id
   *   The reaction ID for this commerce reaction.
   *
   * @return $this
   */
  public function setReactionId($reaction_id);

  /**
   * Executes the reaction.
   *
   * @param array $context
   *   The data objects representing the context of this reaction.
   */
  public function execute(array $context = []);

}
