<?php

namespace Drupal\commerce_reactions\Plugin\CommerceReaction;

use Drupal\commerce_reactions\Plugin\CommerceReactionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Psr\Log\LogLevel;

/**
 * Provides "Log" reaction.
 *
 * @CommerceReaction(
 *   id = "commerce_reactions_log",
 *   label = @Translation("Log"),
 *   category = @Translation("System"),
 *   entity_types = {}
 * )
 */
class Log extends CommerceReactionBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'level' => NULL,
      'message' => NULL,
      'link' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_types = $form_state->getTemporaryValue('entity_types') ?: [];

    $token_tree = $this->treeBuilder->buildRenderable($entity_types);

    $tab_group = implode('][', array_merge($form['#parents'], ['configuration']));

    $form['configuration'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Email configuration'),
    ];

    // To.
    $form['log'] = [
      '#type' => 'details',
      '#title' => $this->t('Log'),
      '#group' => $tab_group,
    ];
    $form['log']['level'] = [
      '#type' => 'select',
      '#title' => $this->t('Level'),
      '#options' => [
        LogLevel::EMERGENCY => $this->t('Emergency'),
        LogLevel::ALERT => $this->t('Alert'),
        LogLevel::CRITICAL => $this->t('Critical'),
        LogLevel::ERROR => $this->t('Error'),
        LogLevel::WARNING => $this->t('Warning'),
        LogLevel::NOTICE => $this->t('Notice'),
        LogLevel::INFO => $this->t('Info'),
        LogLevel::DEBUG => $this->t('Debug'),
      ],
      '#default_value' => $this->getConfiguration()['level'],
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['level']),
    ];
    $form['log']['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => $this->getConfiguration()['message'],
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['message']),
    ];
    $form['log']['link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link'),
      '#default_value' => $this->getConfiguration()['link'],
      '#parents' => array_merge($form['#parents'], ['link']),
    ];
    $form['log']['token_tree'] = $token_tree;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @TODO Add validation.
  }

  /**
   * Logs.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   */
  protected function doExecute(array $contexts) {
    parent::doExecute($contexts);

    $message = $this->tokenManager->replace($this->getConfiguration()['message'], $this->tokenData, $this->tokenOptions);
    $context = [];
    if (!empty($this->getConfiguration()['link'])) {
      $context['link'] = Link::fromTextAndUrl($this->t('Edit'), Url::fromUserInput($this->tokenManager->replace($this->getConfiguration()['link'], $this->tokenData, $this->tokenOptions)))->toString();
    }

    $this->getLogger()->log($this->getConfiguration()['level'], $message, $context);
  }

}
