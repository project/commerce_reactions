<?php

namespace Drupal\commerce_reactions\Plugin\CommerceReaction;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides "Send email" reaction.
 *
 * @CommerceReaction(
 *   id = "commerce_reactions_send_twig_template_email",
 *   label = @Translation("Send twig template email"),
 *   category = @Translation("System"),
 *   entity_types = {}
 * )
 */
class SendTwigTemplateEmail extends SendEmail {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $template_variables = '';
    $contexts = $form_state->getTemporaryValue('contexts') ?: [];
    foreach ($contexts as $context_name => $context) {
      $template_variables .= ' *   - ' . $context_name . ': ' . $context['label'] . PHP_EOL;
    }

    // @TODO Add codemirror support.
    $form['message']['body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => isset($this->getConfiguration()['body']) ? $this->getConfiguration()['body'] : "{#\n/**\n * Email template.\n *\n * Available variables:\n$template_variables */\n#}\n\n",
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['body']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @TODO Add validation.
  }

  /**
   * Builds the email's body.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   *
   * @return string
   *   The built message.
   */
  protected function buildBody(array $contexts) {
    $message['body'] = $this->tokenManager->replace($this->getConfiguration()['body'], $this->tokenData, $this->tokenOptions);

    // Render body using commerce reactions email message template.
    $build = [
      '#type' => 'inline_template',
      '#template' => $message['body'],
      '#context' => $contexts,
    ];
    $theme_name = $this->configuration['theme_name'];

    return trim((string) $this->themeManager->renderPlain($build, $theme_name));
  }

}
