<?php

namespace Drupal\commerce_reactions\Plugin\CommerceReaction;

use Drupal\commerce_reactions\Plugin\CommerceReactionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides "Send twig template email" reaction.
 *
 * @CommerceReaction(
 *   id = "commerce_reactions_send_email",
 *   label = @Translation("Send email"),
 *   category = @Translation("System"),
 *   entity_types = {}
 * )
 */
class SendEmail extends CommerceReactionBase implements ContainerFactoryPluginInterface {

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->setMailManager($container->get('plugin.manager.mail'));

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'to_mail' => [],
      'cc_mail' => [],
      'bcc_mail' => [],
      'from_mail' => NULL,
      'reply_mail' => NULL,
      'subject' => NULL,
      'body' => NULL,
      'theme_name' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $entity_types = $form_state->getTemporaryValue('entity_types') ?: [];

    $token_tree = $this->treeBuilder->buildRenderable($entity_types);
    $tab_group = implode('][', array_merge($form['#parents'], ['configuration']));

    $form['configuration'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Email configuration'),
    ];

    // To.
    $form['to'] = [
      '#type' => 'details',
      '#title' => $this->t('Send to'),
      '#group' => $tab_group,
    ];
    $form['to']['to_mail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('To'),
      '#default_value' => $this->getConfiguration()['to_mail'],
      '#description' => $this->t('The email address or addresses where the message will be sent to, one value per line. The formatting of this string will be validated with the <a href=":link">PHP email validation filter</a>. Some examples are: "user@example.com" or "Another User &lt;another-user@example.com&gt;".', [':link' => 'http://php.net/manual/filter.filters.validate.php']),
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['to_mail']),
    ];
    $form['to']['cc_mail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cc'),
      '#default_value' => $this->getConfiguration()['cc_mail'],
      '#description' => $this->t('The email address or addresses where the message will be sent carbon copy to, one value per line. The formatting of this string will be validated with the <a href=":link">PHP email validation filter</a>. Some examples are: "user@example.com" or "Another User &lt;another-user@example.com&gt;".', [':link' => 'http://php.net/manual/filter.filters.validate.php']),
      '#parents' => array_merge($form['#parents'], ['cc_mail']),
    ];
    $form['to']['bcc_mail'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Bcc'),
      '#default_value' => $this->getConfiguration()['bcc_mail'],
      '#description' => $this->t('The email address or addresses where the message will be sent blind carbon copy to, one value per line. The formatting of this string will be validated with the <a href=":link">PHP email validation filter</a>. Some examples are: "user@example.com" or "Another User &lt;another-user@example.com&gt;".', [':link' => 'http://php.net/manual/filter.filters.validate.php']),
      '#parents' => array_merge($form['#parents'], ['bcc_mail']),
    ];
    $form['to']['token_tree'] = $token_tree;

    // Form.
    $form['from'] = [
      '#type' => 'details',
      '#title' => $this->t('Send from'),
      '#group' => $tab_group,
    ];
    $form['from']['from_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From'),
      '#default_value' => $this->getConfiguration()['from_mail'] ?? '[site:name] <[site:mail]>',
      '#description' => $this->t('The email address or addresses where the message will be sent from. The formatting of this string will be validated with the <a href=":link">PHP email validation filter</a>. Some examples are: "user@example.com" or "User &lt;user@example.com&gt;".', [':link' => 'http://php.net/manual/filter.filters.validate.php']),
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['from_mail']),
    ];
    $form['from']['reply_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply to'),
      '#default_value' => $this->getConfiguration()['reply_mail'],
      '#description' => $this->t('The email address to be used to answer. The formatting of this string will be validated with the <a href=":link">PHP email validation filter</a>. Some examples are: "user@example.com" or "User &lt;user@example.com&gt;".', [':link' => 'http://php.net/manual/filter.filters.validate.php']),
      '#parents' => array_merge($form['#parents'], ['reply_mail']),
    ];
    $form['from']['token_tree'] = $token_tree;

    // Message.
    $form['message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message'),
      '#group' => $tab_group,
    ];
    $form['message']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $this->getConfiguration()['subject'],
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['subject']),
    ];
    $form['message']['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => isset($this->getConfiguration()['body']['value']) ? $this->getConfiguration()['body']['value'] : NULL,
      '#format' => isset($this->getConfiguration()['body']['format']) ? $this->getConfiguration()['body']['format'] : filter_default_format(),
      '#required' => TRUE,
      '#parents' => array_merge($form['#parents'], ['body']),
    ];
    $form['message']['token_tree'] = $token_tree;

    // Additional.
    $form['additional'] = [
      '#type' => 'details',
      '#title' => $this->t('Additional'),
      '#group' => $tab_group,
    ];
    $form['additional']['theme_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme to render this email'),
      '#description' => $this->t('Select the theme that will be used to render this email.'),
      '#options' => $this->themeManager->getThemeNames(),
      '#empty_value' => '',
      '#empty_option' => $this->t('- Site default -'),
      '#default_value' => $this->getConfiguration()['theme_name'],
      '#parents' => array_merge($form['#parents'], ['theme_name']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @TODO Add validation.
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    if (!empty($this->getConfiguration()['theme_name'])) {
      $dependencies['theme'][] = $this->getConfiguration()['theme_name'];
    }

    return $dependencies;
  }

  /**
   * Send an email.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   */
  protected function doExecute(array $contexts) {
    parent::doExecute($contexts);

    // Prepare the message, add the body and subject and send it.
    if (($message = $this->prepareEmail($contexts)) !== NULL) {
      $message['subject'] = $this->tokenManager->replace($this->getConfiguration()['subject'], $this->tokenData, $this->tokenOptions);
      $message['body'] = $this->buildBody($contexts);

      $this->sendEmailMessage($message);
    }
  }

  /**
   * Prepares an email message.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   *
   * @return array|null
   *   The prepared email or NULL if it couldn't be prepared.
   */
  protected function prepareEmail(array $contexts) {
    $to = $cc = $bcc = $from = $reply_to = NULL;
    if (!empty($this->getConfiguration()['to_mail'])) {
      $to = array_filter(array_map('trim', explode(PHP_EOL, $this->tokenManager->replace($this->getConfiguration()['to_mail'], $this->tokenData, $this->tokenOptions))));;
    }
    if (!empty($this->getConfiguration()['cc_mail'])) {
      $cc = array_filter(array_map('trim', explode(PHP_EOL, $this->tokenManager->replace($this->getConfiguration()['cc_mail'], $this->tokenData, $this->tokenOptions))));;
    }
    if (!empty($this->getConfiguration()['bcc_mail'])) {
      $bcc = array_filter(array_map('trim', explode(PHP_EOL, $this->tokenManager->replace($this->getConfiguration()['bcc_mail'], $this->tokenData, $this->tokenOptions))));;
    }
    if (!empty($this->getConfiguration()['from_mail'])) {
      $from = $this->tokenManager->replace($this->getConfiguration()['from_mail'], $this->tokenData, $this->tokenOptions);
    }
    if (!empty($this->getConfiguration()['reply_mail'])) {
      $reply_to = $this->tokenManager->replace($this->getConfiguration()['reply_mail'], $this->tokenData, $this->tokenOptions);
    }

    foreach (['to', 'cc', 'bcc', 'from', 'reply_to'] as $item) {
      if (!is_array($$item)) {
        $$item = [$$item];
      }

      foreach ($$item as $key => $mail) {
        if (empty($mail) || (!filter_var($mail, FILTER_VALIDATE_EMAIL) && !preg_match('/^\s*(.+?)\s*<\s*([^>]+)\s*>$/', $mail))) {
          unset(${$item}[$key]);
        }
      }

      $$item = implode(', ', $$item);
    }

    // Don't send the message if To, CC, and BCC are empty.
    if (empty($to) && empty($cc) && empty($bcc)) {
      $this->getLogger()->warning('Unable to sent email for %reaction reaction because a <em>To</em>, <em>CC</em>, or <em>BCC</em> email was not provided.', ['%reaction' => $this->getLabel()]);

      return NULL;
    }

    // Prepare the message.
    $message['to'] = $to;
    $message['cc'] = $cc;
    $message['bcc'] = $bcc;
    $message['reply-to'] = $reply_to;
    $message['from'] = $from;
    // Remove empty recipients.
    $message = array_filter($message);

    return $message;
  }

  /**
   * Builds the email's body.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   *
   * @return string
   *   The built message.
   */
  protected function buildBody(array $contexts) {
    $message['body'] = $this->tokenManager->replace($this->getConfiguration()['body']['value'], $this->tokenData, $this->tokenOptions);

    // Render body using commerce reactions email message template.
    $build = [
      '#theme' => 'commerce_reactions_email',
      '#message' => [
        '#type' => 'processed_text',
        '#text' => $message['body'],
        '#format' => $this->getConfiguration()['body']['format'],
      ],
      '#context' => $this->tokenData,
      '#reaction' => $this,
    ];
    $theme_name = $this->configuration['theme_name'];

    return trim((string) $this->themeManager->renderPlain($build, $theme_name));
  }

  /**
   * Sends the email message.
   *
   * @param array $message
   *   The message array structure containing all details of the message.
   */
  protected function sendEmailMessage(array $message) {
    // Set a unique key for this mail.
    $key = $this->getPluginId() . '_' . $this->getReactionId();

    // Send message.
    $message = $this->mailManager->mail('commerce_reactions', $key, $message['to'], $this->languageManager->getCurrentLanguage()->getId(), $message, $message['from']);
    if ($message['result']) {
      $this->getLogger()->notice('Successfully sent email to %recipient.', ['%recipient' => $message['to']]);
    }
    else {
      $this->getLogger()->error('Unable to sent email to %recipient.', ['%recipient' => $message['to']]);
    }
  }

  /**
   * Sets the mail manager service.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager service.
   *
   * @return $this
   */
  protected function setMailManager(MailManagerInterface $mail_manager) {
    $this->mailManager = $mail_manager;

    return $this;
  }

}
