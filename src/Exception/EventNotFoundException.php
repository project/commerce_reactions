<?php

namespace Drupal\commerce_reactions\Exception;

/**
 * Plugin exception class to be thrown when an event could not be found.
 */
class EventNotFoundException extends \Exception {

  /**
   * Construct an EventGroupNotFoundException exception.
   *
   * @param string $event_id
   *   The event ID that was not found.
   * @param string $message
   *   (Optional) The Exception message to throw.
   * @param int $code
   *   (Optional) The Exception code.
   * @param \Exception $previous
   *   (Optional) The previous throwable used for the exception chaining.
   */
  public function __construct($event_id, $message = '', $code = 0, \Exception $previous = NULL) {
    if (empty($message)) {
      $message = sprintf("Event ID '%s' was not found.", $event_id);
    }

    parent::__construct($message, $code, $previous);
  }

}
