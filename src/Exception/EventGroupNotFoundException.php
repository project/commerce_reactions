<?php

namespace Drupal\commerce_reactions\Exception;

/**
 * Plugin exception class to be thrown when an event group could not be found.
 */
class EventGroupNotFoundException extends \Exception {

  /**
   * Construct an EventGroupNotFoundException exception.
   *
   * @param string $group_id
   *   The event group ID that was not found.
   * @param string $message
   *   (Optional) The Exception message to throw.
   * @param int $code
   *   (Optional) The Exception code.
   * @param \Exception $previous
   *   (Optional) The previous throwable used for the exception chaining.
   */
  public function __construct($group_id, $message = '', $code = 0, \Exception $previous = NULL) {
    if (empty($message)) {
      $message = sprintf("Event group ID '%s' was not found.", $group_id);
    }

    parent::__construct($message, $code, $previous);
  }

}
