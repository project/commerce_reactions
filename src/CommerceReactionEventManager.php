<?php

namespace Drupal\commerce_reactions;

use Drupal\commerce\Event\CommerceEvents;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\commerce_checkout\Event\CheckoutEvents;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\commerce_payment\Event\PaymentEvents;
use Drupal\commerce_price\Event\PriceEvents;
use Drupal\commerce_product\Event\ProductEvents;
use Drupal\commerce_promotion\Event\PromotionEvents;
use Drupal\commerce_reactions\Exception\EventGroupNotFoundException;
use Drupal\commerce_reactions\Exception\EventNotFoundException;
use Drupal\commerce_store\Event\StoreEvents;
use Drupal\commerce_tax\Event\TaxEvents;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\state_machine\WorkflowManagerInterface;

/**
 * Defines a class to manage commerce reactions events.
 */
class CommerceReactionEventManager implements CommerceReactionEventManagerInterface {

  use StringTranslationTrait;
  use UseCacheBackendTrait;

  /**
   * The cache key.
   *
   * @var string
   */
  protected $cacheKey = 'commerce_reactions.events';

  /**
   * An array of cache tags to use for the cached definitions.
   *
   * @var array
   */
  protected $cacheTags = ['commerce_reactions'];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Cached event groups array.
   *
   * @var array
   */
  protected $events = NULL;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The workflow manager.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * Constructs a new CommerceReactionEventManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cacheBackend = $cache_backend;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Sets the workflow manager.
   *
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   *
   * @return $this
   */
  public function setWorkflowManager(WorkflowManagerInterface $workflow_manager) {
    $this->workflowManager = $workflow_manager;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvent($event_id, $exception_on_invalid = TRUE) {
    $events = $this->getEvents();
    foreach ($events as $group) {
      if (array_key_exists($event_id, $group['events'])) {
        return $group['events'][$event_id];
      }
    }
    if ($exception_on_invalid) {
      throw new EventNotFoundException($event_id);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventEntityContexts($event_id, $exception_on_invalid = TRUE) {
    $entity_contexts = [];

    $event = $this->getEvent($event_id, $exception_on_invalid);
    if ($event) {
      foreach ($event['contexts'] as $context) {
        if (!empty($context['entity_id']) && $this->entityTypeManager->hasDefinition($context['entity_id'])) {
          $entity_contexts[] = $context['entity_id'];
        }
      }
    }

    return $entity_contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvents() {
    $events = $this->getCachedEvents();
    if (!isset($events)) {
      $events = $this->findEvents();
      $this->setCachedEvents($events);
    }

    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventsGroup($group_id, $exception_on_invalid = TRUE) {
    $events = $this->getEvents();
    if (array_key_exists($group_id, $events)) {
      return $events[$group_id];
    }
    elseif ($exception_on_invalid) {
      throw new EventGroupNotFoundException($group_id);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventOptions() {
    $options = [];

    foreach ($this->getEvents() as $group) {
      $group_name = $group['label']->__toString();
      $options[$group_name] = [];
      foreach ($group['events'] as $event_id => $event) {
        // @TODO Use a human-readable name.
        $options[$group_name][$event_id] = $event['id'];
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    if ($this->cacheBackend) {
      if ($this->cacheTags) {
        // Use the cache tags to clear the cache.
        Cache::invalidateTags($this->cacheTags);
      }
      else {
        $this->cacheBackend->delete($this->cacheKey);
      }
    }
    $this->events = NULL;
  }

  /**
   * Returns the cached plugin definitions of the decorated discovery class.
   *
   * @return array|null
   *   On success this will return an array of plugin definitions. On failure
   *   this should return NULL, indicating to other methods that this has not
   *   yet been defined. Success with no values should return as an empty array
   *   and would actually be returned by the getDefinitions() method.
   */
  protected function getCachedEvents() {
    if (!isset($this->events) && $cache = $this->cacheGet($this->cacheKey)) {
      $this->events = $cache->data;
    }
    return $this->events;
  }

  /**
   * Sets a cache of plugin definitions for the decorated discovery class.
   *
   * @param array $events
   *   List of events to store in cache.
   */
  protected function setCachedEvents(array $events) {
    $this->cacheSet($this->cacheKey, $events, Cache::PERMANENT, $this->cacheTags);
    $this->events = $events;
  }

  /**
   * Finds event groups.
   *
   * @return array
   *   List of definitions to store in cache.
   */
  protected function findEvents() {
    $events = $this->getCoreEntityEvents();

    $events += $this->moduleHandler->invokeAll('commerce_reactions_events');
    $this->moduleHandler->alter('commerce_reactions_events', $events);

    // @TODO Validate events.
    return $events;
  }

  /**
   * Returns all Drupal Commerce core events.
   *
   * @return array
   *   A core event list.
   */
  private function getCoreEntityEvents() {
    $events = [
      'commerce' => [
        'label' => $this->t('Commerce'),
        'events' => [
          CommerceEvents::FILTER_CONDITIONS => [
            'id' => CommerceEvents::FILTER_CONDITIONS,
            'class' => '\Drupal\commerce\Event\FilterConditionsEvent',
            'provider' => 'commerce',
            'contexts' => [
              'definitions' => [
                'label' => $this->t('Condition definitions'),
                'getter' => 'getDefinitions',
                'return' => 'array',
              ],
              'parent_entity_type_id' => [
                'label' => $this->t('Parent entity type ID'),
                'getter' => 'getParentEntityTypeId',
                'return' => 'string',
              ],
            ],
          ],
          CommerceEvents::REFERENCEABLE_PLUGIN_TYPES => [
            'id' => CommerceEvents::REFERENCEABLE_PLUGIN_TYPES,
            'class' => '\Drupal\commerce\Event\ReferenceablePluginTypesEvent',
            'provider' => 'commerce',
            'contexts' => [
              'plugin_types' => [
                'label' => $this->t('Plugin types'),
                'getter' => 'getPluginTypes',
                'return' => 'array',
              ],
            ],
          ],
        ],
      ],
    ];

    if ($this->moduleHandler->moduleExists('commerce_cart')) {
      $events += [
        'commerce_cart' => [
          'label' => $this->t('Commerce Cart'),
          'events' => [
            CartEvents::CART_EMPTY => [
              'id' => CartEvents::CART_EMPTY,
              'class' => '\Drupal\commerce_cart\Event\CartEmptyEvent',
              'provider' => 'commerce',
              'contexts' => [
                'cart' => [
                  'label' => $this->t('Cart'),
                  'getter' => 'getCart',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
                'order_items' => [
                  'label' => $this->t('Order Items'),
                  'getter' => 'getOrderItems',
                  'return' => 'array',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            CartEvents::CART_ENTITY_ADD => [
              'id' => CartEvents::CART_ENTITY_ADD,
              'class' => '\Drupal\commerce_cart\Event\CartEntityAddEvent',
              'provider' => 'commerce',
              'contexts' => [
                'cart' => [
                  'label' => $this->t('Cart'),
                  'getter' => 'getCart',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
                'entity' => [
                  'label' => $this->t('Added entity'),
                  'getter' => 'getEntity',
                  'return' => 'object',
                ],
                'quantity' => [
                  'getter' => 'getQuantity',
                  'label' => $this->t('Quantity'),
                  'return' => 'float',
                ],
                'order_item' => [
                  'label' => $this->t('Order Item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            CartEvents::CART_ORDER_ITEM_REMOVE => [
              'id' => CartEvents::CART_ORDER_ITEM_REMOVE,
              'class' => '\Drupal\commerce_cart\Event\CartOrderItemRemoveEvent',
              'provider' => 'commerce',
              'contexts' => [
                'cart' => [
                  'label' => $this->t('Cart'),
                  'getter' => 'getCart',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
                'order_item' => [
                  'label' => $this->t('Order Item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            CartEvents::CART_ORDER_ITEM_UPDATE => [
              'id' => CartEvents::CART_ORDER_ITEM_UPDATE,
              'class' => '\Drupal\commerce_cart\Event\CartOrderItemUpdateEvent',
              'provider' => 'commerce',
              'contexts' => [
                'cart' => [
                  'label' => $this->t('Cart'),
                  'getter' => 'getCart',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
                'order_item' => [
                  'label' => $this->t('Order Item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
                'original_order_item' => [
                  'label' => $this->t('Original Order Item'),
                  'getter' => 'getOriginalOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            CartEvents::ORDER_ITEM_COMPARISON_FIELDS => [
              'id' => CartEvents::ORDER_ITEM_COMPARISON_FIELDS,
              'class' => '\Drupal\commerce_cart\Event\OrderItemComparisonFieldsEvent',
              'provider' => 'commerce',
              'contexts' => [
                'comparison_fields' => [
                  'label' => $this->t('Order Item'),
                  'getter' => 'getComparisonFields',
                  'return' => 'array',
                ],
                'order_item' => [
                  'label' => $this->t('Order Item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('commerce_checkout')) {
      $events += [
        'commerce_checkout' => [
          'label' => $this->t('Commerce Checkout'),
          'events' => [
            CheckoutEvents::COMPLETION_REGISTER => [
              'id' => CheckoutEvents::COMPLETION_REGISTER,
              'class' => '\Drupal\commerce_checkout\Event\CheckoutCompletionRegisterEvent',
              'provider' => 'commerce_checkout',
              'contexts' => [
                'account' => [
                  'label' => $this->t('Account'),
                  'getter' => 'getAccount',
                  'return' => 'object',
                  'entity_id' => 'user',
                ],
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
                'url' => [
                  'label' => $this->t('Redirect URL'),
                  'getter' => 'getRedirectUrl',
                  'return' => 'object',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('commerce_order')) {
      $events += [
        'commerce_order' => [
          'label' => $this->t('Commerce Order'),
          'events' => [
            OrderEvents::ORDER_ASSIGN => [
              'id' => OrderEvents::ORDER_ASSIGN,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_CREATE => [
              'id' => OrderEvents::ORDER_CREATE,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_DELETE => [
              'id' => OrderEvents::ORDER_DELETE,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_INSERT => [
              'id' => OrderEvents::ORDER_INSERT,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_LOAD => [
              'id' => OrderEvents::ORDER_LOAD,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_PAID => [
              'id' => OrderEvents::ORDER_PAID,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_PREDELETE => [
              'id' => OrderEvents::ORDER_PREDELETE,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_PRESAVE => [
              'id' => OrderEvents::ORDER_PRESAVE,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_UPDATE => [
              'id' => OrderEvents::ORDER_UPDATE,
              'class' => '\Drupal\commerce_order\Event\OrderEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
              ],
            ],
            OrderEvents::ORDER_ITEM_CREATE => [
              'id' => OrderEvents::ORDER_ITEM_CREATE,
              'class' => '\Drupal\commerce_order\Event\OrderItemEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order_item' => [
                  'label' => $this->t('Order item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            OrderEvents::ORDER_ITEM_DELETE => [
              'id' => OrderEvents::ORDER_ITEM_DELETE,
              'class' => '\Drupal\commerce_order\Event\OrderItemEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order_item' => [
                  'label' => $this->t('Order item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            OrderEvents::ORDER_ITEM_INSERT => [
              'id' => OrderEvents::ORDER_ITEM_INSERT,
              'class' => '\Drupal\commerce_order\Event\OrderItemEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order_item' => [
                  'label' => $this->t('Order item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            OrderEvents::ORDER_ITEM_LOAD => [
              'id' => OrderEvents::ORDER_ITEM_LOAD,
              'class' => '\Drupal\commerce_order\Event\OrderItemEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order_item' => [
                  'label' => $this->t('Order item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            OrderEvents::ORDER_ITEM_PREDELETE => [
              'id' => OrderEvents::ORDER_ITEM_PREDELETE,
              'class' => '\Drupal\commerce_order\Event\OrderItemEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order_item' => [
                  'label' => $this->t('Order item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            OrderEvents::ORDER_ITEM_PRESAVE => [
              'id' => OrderEvents::ORDER_ITEM_PRESAVE,
              'class' => '\Drupal\commerce_order\Event\OrderItemEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order_item' => [
                  'label' => $this->t('Order item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
            OrderEvents::ORDER_ITEM_UPDATE => [
              'id' => OrderEvents::ORDER_ITEM_UPDATE,
              'class' => '\Drupal\commerce_order\Event\OrderItemEvent',
              'provider' => 'commerce_order',
              'contexts' => [
                'order_item' => [
                  'label' => $this->t('Order item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
          ],
        ],
      ];

      $groups = $this->workflowManager->getGroupedLabels('commerce_order');
      $group = reset($groups);
      foreach ($group as $workflow_id => $workflow) {
        try {
          $workflow_definition = $this->workflowManager->createInstance($workflow_id);
          foreach ($workflow_definition->getTransitions() as $transition_id => $transition) {
            foreach (['pre_transition', 'post_transition'] as $phase) {
              $events['commerce_order']['events']['commerce_order.' . $transition_id . '.' . $phase] = [
                'id' => 'commerce_order.' . $transition_id . '.' . $phase,
                'class' => '\Drupal\state_machine\Event\WorkflowTransitionEvent',
                'provider' => 'commerce_order',
                'contexts' => [
                  'workflow' => [
                    'label' => $this->t('Workflow'),
                    'getter' => 'getWorkflow',
                    'return' => 'object',
                  ],
                  'order' => [
                    'label' => $this->t('Order'),
                    'getter' => 'getEntity',
                    'return' => 'object',
                    'entity_id' => 'commerce_order',
                  ],
                  'field_name' => [
                    'label' => $this->t('State field name'),
                    'getter' => 'getFieldName',
                    'return' => 'string',
                  ],
                  'from_state' => [
                    'label' => $this->t('From state'),
                    'getter' => 'getFromState',
                    'return' => 'string',
                  ],
                  'to_state' => [
                    'label' => $this->t('To state'),
                    'getter' => 'getToState',
                    'return' => 'string',
                  ],
                ],
              ];
            }
          }
        }
        catch (\Exception $exception) {
          // Nothing to do.
        }
      }
    }

    if ($this->moduleHandler->moduleExists('commerce_payment')) {
      $events += [
        'commerce_payment' => [
          'label' => $this->t('Commerce Payment'),
          'events' => [
            PaymentEvents::FILTER_PAYMENT_GATEWAYS => [
              'id' => PaymentEvents::FILTER_PAYMENT_GATEWAYS,
              'class' => '\Drupal\commerce_payment\Event\FilterPaymentGatewaysEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'order' => [
                  'label' => $this->t('Order'),
                  'getter' => 'getOrder',
                  'return' => 'object',
                  'entity_id' => 'commerce_order',
                ],
                'payment_gateways' => [
                  'label' => $this->t('Payment gateways'),
                  'getter' => 'getPaymentGateways',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment_gateway',
                ],
              ],
            ],
            PaymentEvents::PAYMENT_LOAD => [
              'id' => PaymentEvents::PAYMENT_LOAD,
              'class' => '\Drupal\commerce_payment\Event\PaymentEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'payment' => [
                  'label' => $this->t('Payment'),
                  'getter' => 'getPayment',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment',
                ],
              ],
            ],
            PaymentEvents::PAYMENT_CREATE => [
              'id' => PaymentEvents::PAYMENT_CREATE,
              'class' => '\Drupal\commerce_payment\Event\PaymentEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'payment' => [
                  'label' => $this->t('Payment'),
                  'getter' => 'getPayment',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment',
                ],
              ],
            ],
            PaymentEvents::PAYMENT_PRESAVE => [
              'id' => PaymentEvents::PAYMENT_PRESAVE,
              'class' => '\Drupal\commerce_payment\Event\PaymentEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'payment' => [
                  'label' => $this->t('Payment'),
                  'getter' => 'getPayment',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment',
                ],
              ],
            ],
            PaymentEvents::PAYMENT_INSERT => [
              'id' => PaymentEvents::PAYMENT_INSERT,
              'class' => '\Drupal\commerce_payment\Event\PaymentEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'payment' => [
                  'label' => $this->t('Payment'),
                  'getter' => 'getPayment',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment',
                ],
              ],
            ],
            PaymentEvents::PAYMENT_UPDATE => [
              'id' => PaymentEvents::PAYMENT_UPDATE,
              'class' => '\Drupal\commerce_payment\Event\PaymentEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'payment' => [
                  'label' => $this->t('Payment'),
                  'getter' => 'getPayment',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment',
                ],
              ],
            ],
            PaymentEvents::PAYMENT_PREDELETE => [
              'id' => PaymentEvents::PAYMENT_PREDELETE,
              'class' => '\Drupal\commerce_payment\Event\PaymentEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'payment' => [
                  'label' => $this->t('Payment'),
                  'getter' => 'getPayment',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment',
                ],
              ],
            ],
            PaymentEvents::PAYMENT_DELETE => [
              'id' => PaymentEvents::PAYMENT_DELETE,
              'class' => '\Drupal\commerce_payment\Event\PaymentEvent',
              'provider' => 'commerce_payment',
              'contexts' => [
                'payment' => [
                  'label' => $this->t('Payment'),
                  'getter' => 'getPayment',
                  'return' => 'array',
                  'entity_id' => 'commerce_payment',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('commerce_price')) {
      $events += [
        'commerce_price' => [
          'label' => $this->t('Commerce Price'),
          'events' => [
            PriceEvents::NUMBER_FORMAT => [
              'id' => PriceEvents::NUMBER_FORMAT,
              'class' => '\Drupal\commerce_price\Event\NumberFormatDefinitionEvent',
              'provider' => 'commerce_price',
              'contexts' => [
                'number_format_definition' => [
                  'label' => $this->t('Number format definition'),
                  'getter' => 'getDefinition',
                  'return' => 'array',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('commerce_product')) {
      $events += [
        'commerce_product' => [
          'label' => $this->t('Commerce Product'),
          'events' => [
            ProductEvents::FILTER_VARIATIONS => [
              'id' => ProductEvents::FILTER_VARIATIONS,
              'class' => '\Drupal\commerce_product\Event\FilterVariationsEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
                'product_variations' => [
                  'label' => $this->t('Enabled product variations'),
                  'getter' => 'getVariations',
                  'return' => 'array',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_CREATE => [
              'id' => ProductEvents::PRODUCT_CREATE,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_DELETE => [
              'id' => ProductEvents::PRODUCT_DELETE,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_INSERT => [
              'id' => ProductEvents::PRODUCT_INSERT,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_LOAD => [
              'id' => ProductEvents::PRODUCT_LOAD,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_PREDELETE => [
              'id' => ProductEvents::PRODUCT_PREDELETE,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_PRESAVE => [
              'id' => ProductEvents::PRODUCT_PRESAVE,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_TRANSLATION_DELETE => [
              'id' => ProductEvents::PRODUCT_TRANSLATION_DELETE,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_TRANSLATION_INSERT => [
              'id' => ProductEvents::PRODUCT_TRANSLATION_INSERT,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_UPDATE => [
              'id' => ProductEvents::PRODUCT_UPDATE,
              'class' => '\Drupal\commerce_product\Event\ProductEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product' => [
                  'label' => $this->t('Product'),
                  'getter' => 'getProduct',
                  'return' => 'object',
                  'entity_id' => 'commerce_product',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_AJAX_CHANGE => [
              'id' => ProductEvents::PRODUCT_VARIATION_AJAX_CHANGE,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_CREATE => [
              'id' => ProductEvents::PRODUCT_VARIATION_CREATE,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_DELETE => [
              'id' => ProductEvents::PRODUCT_VARIATION_DELETE,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_INSERT => [
              'id' => ProductEvents::PRODUCT_VARIATION_INSERT,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_LOAD => [
              'id' => ProductEvents::PRODUCT_VARIATION_LOAD,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_PREDELETE => [
              'id' => ProductEvents::PRODUCT_VARIATION_PREDELETE,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_PRESAVE => [
              'id' => ProductEvents::PRODUCT_VARIATION_PRESAVE,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_TRANSLATION_DELETE => [
              'id' => ProductEvents::PRODUCT_VARIATION_TRANSLATION_DELETE,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_TRANSLATION_INSERT => [
              'id' => ProductEvents::PRODUCT_VARIATION_TRANSLATION_INSERT,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
            ProductEvents::PRODUCT_VARIATION_UPDATE => [
              'id' => ProductEvents::PRODUCT_VARIATION_UPDATE,
              'class' => '\Drupal\commerce_product\Event\ProductVariationEvent',
              'provider' => 'commerce_product',
              'contexts' => [
                'product_variation' => [
                  'label' => $this->t('Product variation'),
                  'getter' => 'getProductVariation',
                  'return' => 'object',
                  'entity_id' => 'commerce_product_variation',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('commerce_promotion')) {
      $events += [
        'commerce_promotion' => [
          'label' => $this->t('Commerce Promotion'),
          'events' => [
            PromotionEvents::COUPON_CREATE => [
              'id' => PromotionEvents::COUPON_CREATE,
              'class' => '\Drupal\commerce_promotion\Event\CouponEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'coupon' => [
                  'label' => $this->t('Coupon'),
                  'getter' => 'getCoupon',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion_coupon',
                ],
              ],
            ],
            PromotionEvents::COUPON_DELETE => [
              'id' => PromotionEvents::COUPON_DELETE,
              'class' => '\Drupal\commerce_promotion\Event\CouponEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'coupon' => [
                  'label' => $this->t('Coupon'),
                  'getter' => 'getCoupon',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion_coupon',
                ],
              ],
            ],
            PromotionEvents::COUPON_INSERT => [
              'id' => PromotionEvents::COUPON_INSERT,
              'class' => '\Drupal\commerce_promotion\Event\CouponEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'coupon' => [
                  'label' => $this->t('Coupon'),
                  'getter' => 'getCoupon',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion_coupon',
                ],
              ],
            ],
            PromotionEvents::COUPON_LOAD => [
              'id' => PromotionEvents::COUPON_LOAD,
              'class' => '\Drupal\commerce_promotion\Event\CouponEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'coupon' => [
                  'label' => $this->t('Coupon'),
                  'getter' => 'getCoupon',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion_coupon',
                ],
              ],
            ],
            PromotionEvents::COUPON_PREDELETE => [
              'id' => PromotionEvents::COUPON_PREDELETE,
              'class' => '\Drupal\commerce_promotion\Event\CouponEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'coupon' => [
                  'label' => $this->t('Coupon'),
                  'getter' => 'getCoupon',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion_coupon',
                ],
              ],
            ],
            PromotionEvents::COUPON_PRESAVE => [
              'id' => PromotionEvents::COUPON_PRESAVE,
              'class' => '\Drupal\commerce_promotion\Event\CouponEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'coupon' => [
                  'label' => $this->t('Coupon'),
                  'getter' => 'getCoupon',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion_coupon',
                ],
              ],
            ],
            PromotionEvents::COUPON_UPDATE => [
              'id' => PromotionEvents::COUPON_UPDATE,
              'class' => '\Drupal\commerce_promotion\Event\CouponEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'coupon' => [
                  'label' => $this->t('Coupon'),
                  'getter' => 'getCoupon',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion_coupon',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_CREATE => [
              'id' => PromotionEvents::PROMOTION_CREATE,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_DELETE => [
              'id' => PromotionEvents::PROMOTION_DELETE,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_INSERT => [
              'id' => PromotionEvents::PROMOTION_INSERT,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_LOAD => [
              'id' => PromotionEvents::PROMOTION_LOAD,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_PREDELETE => [
              'id' => PromotionEvents::PROMOTION_PREDELETE,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_PRESAVE => [
              'id' => PromotionEvents::PROMOTION_PRESAVE,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_TRANSLATION_DELETE => [
              'id' => PromotionEvents::PROMOTION_TRANSLATION_DELETE,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_TRANSLATION_INSERT => [
              'id' => PromotionEvents::PROMOTION_TRANSLATION_INSERT,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
            PromotionEvents::PROMOTION_UPDATE => [
              'id' => PromotionEvents::PROMOTION_UPDATE,
              'class' => '\Drupal\commerce_promotion\Event\PromotionEvent',
              'provider' => 'commerce_promotion',
              'contexts' => [
                'promotion' => [
                  'label' => $this->t('Promotion'),
                  'getter' => 'getPromotion',
                  'return' => 'object',
                  'entity_id' => 'commerce_promotion',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('commerce_store')) {
      $events += [
        'commerce_store' => [
          'label' => $this->t('Commerce Store'),
          'events' => [
            StoreEvents::STORE_CREATE => [
              'id' => StoreEvents::STORE_CREATE,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
            StoreEvents::STORE_DELETE => [
              'id' => StoreEvents::STORE_DELETE,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
            StoreEvents::STORE_INSERT => [
              'id' => StoreEvents::STORE_INSERT,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
            StoreEvents::STORE_LOAD => [
              'id' => StoreEvents::STORE_LOAD,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
            StoreEvents::STORE_PREDELETE => [
              'id' => StoreEvents::STORE_PREDELETE,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [],
            ],
            StoreEvents::STORE_PRESAVE => [
              'id' => StoreEvents::STORE_PRESAVE,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
            StoreEvents::STORE_TRANSLATION_DELETE => [
              'id' => StoreEvents::STORE_TRANSLATION_DELETE,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
            StoreEvents::STORE_TRANSLATION_INSERT => [
              'id' => StoreEvents::STORE_TRANSLATION_INSERT,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
            StoreEvents::STORE_UPDATE => [
              'id' => StoreEvents::STORE_UPDATE,
              'class' => '\Drupal\commerce_store\Event\StoreEvent',
              'provider' => 'commerce_store',
              'contexts' => [
                'store' => [
                  'label' => $this->t('Store'),
                  'getter' => 'getStore',
                  'return' => 'object',
                  'entity_id' => 'commerce_store',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    if ($this->moduleHandler->moduleExists('commerce_tax')) {
      $events += [
        'commerce_tax' => [
          'label' => $this->t('Commerce Tax'),
          'events' => [
            TaxEvents::CUSTOMER_PROFILE => [
              'id' => TaxEvents::CUSTOMER_PROFILE,
              'class' => '\Drupal\commerce_tax\Event\CustomerProfileEvent',
              'provider' => 'commerce_tax',
              'contexts' => [
                'customer_profile' => [
                  'label' => $this->t('Customer profile'),
                  'getter' => 'getCustomerProfile',
                  'return' => 'object',
                  'entity_id' => 'profile',
                ],
                'order_item' => [
                  'label' => $this->t('Order Item'),
                  'getter' => 'getOrderItem',
                  'return' => 'object',
                  'entity_id' => 'commerce_order_item',
                ],
              ],
            ],
          ],
        ],
      ];
    }

    return $events;
  }

}
