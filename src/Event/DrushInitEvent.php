<?php

namespace Drupal\commerce_reactions\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Event that is fired when Drush tasks are performed.
 *
 * @see commerce_reactions_drush_init()
 */
class DrushInitEvent extends GenericEvent {
}
