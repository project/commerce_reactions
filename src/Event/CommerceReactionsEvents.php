<?php

namespace Drupal\commerce_reactions\Event;

/**
 * Defines events for the Commerce Stripe module.
 */
class CommerceReactionsEvents {

  /**
   * Name of the event fired when Drush tasks are performed.
   *
   * @Event
   *
   * @see \Drupal\commerce_reactions\Event\DrushInitEvent
   */
  const DRUSH_INIT = 'commerce_reactions.drush_init';

}
