<?php

namespace Drupal\commerce_reactions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the interface for commerce reactions.
 */
interface ReactionInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Gets the commerce reaction event name.
   *
   * @return string
   *   The commerce reaction event name.
   */
  public function getEvent();

  /**
   * Sets the commerce reaction event name.
   *
   * @param string $event
   *   The commerce reaction event name.
   *
   * @return $this
   */
  public function setEvent($event);

  /**
   * Gets the commerce reaction plugin.
   *
   * @return \Drupal\commerce_reactions\Plugin\CommerceReactionInterface
   *   The commerce reaction plugin.
   */
  public function getPlugin();

  /**
   * Gets the commerce reaction plugin ID.
   *
   * @return string
   *   The commerce reaction plugin ID.
   */
  public function getPluginId();

  /**
   * Sets the commerce reaction plugin ID.
   *
   * @param string $plugin_id
   *   The commerce reaction plugin ID.
   *
   * @return $this
   */
  public function setPluginId($plugin_id);

  /**
   * Gets the commerce reaction plugin configuration.
   *
   * @return array
   *   The commerce reaction plugin configuration.
   */
  public function getPluginConfiguration();

  /**
   * Sets the commerce reaction plugin configuration.
   *
   * @param array $configuration
   *   The commerce reaction plugin configuration.
   *
   * @return $this
   */
  public function setPluginConfiguration(array $configuration);

  /**
   * Gets the commerce reaction conditions.
   *
   * @param string $context_name
   *   The specific context's key.
   *
   * @return \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface[]
   *   The commerce reaction conditions.
   */
  public function getConditions($context_name = NULL);

  /**
   * Gets the commerce reaction condition operator.
   *
   * @param string $context_name
   *   The specific context's key.
   *
   * @return string
   *   The condition operator. Possible values: AND, OR.
   */
  public function getConditionsOperator($context_name);

  /**
   * Sets the commerce reaction condition operator.
   *
   * @param string $context_name
   *   The specific context's key.
   * @param string $condition_operator
   *   The condition operator.
   *
   * @return $this
   */
  public function setConditionsOperator($context_name, $condition_operator);

  /**
   * Checks whether the commerce reaction applies to the given entity.
   *
   * Ensures that the conditions pass.
   *
   * @param array $contexts
   *   The data objects representing the context of this reaction.
   *
   * @return bool
   *   TRUE if commerce reaction applies, FALSE otherwise.
   */
  public function applies(array $contexts);

}
