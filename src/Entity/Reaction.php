<?php

namespace Drupal\commerce_reactions\Entity;

use Drupal\commerce\CommerceSinglePluginCollection;
use Drupal\commerce\ConditionGroup;
use Drupal\commerce\Plugin\Commerce\Condition\ParentEntityAwareInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the commerce reaction entity class.
 *
 * @ConfigEntityType(
 *   id = "commerce_reaction",
 *   label = @Translation("Commerce Reaction"),
 *   label_collection = @Translation("Commerce Reactions"),
 *   label_singular = @Translation("commerce reaction"),
 *   label_plural = @Translation("commerce reactions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count commerce reaction",
 *     plural = "@count commerce reactions",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\commerce_reactions\ReactionStorage",
 *     "list_builder" = "Drupal\commerce_reactions\ReactionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_reactions\Form\ReactionForm",
 *       "edit" = "Drupal\commerce_reactions\Form\ReactionForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "reaction",
 *   admin_permission = "administer commerce_reactions",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "event",
 *     "plugin",
 *     "configuration",
 *     "conditions"
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/config/store/reactions/add",
 *     "edit-form" = "/admin/commerce/config/store/reactions/manage/{commerce_reaction}",
 *     "delete-form" = "/admin/commerce/config/store/reactions/manage/{commerce_reaction}/delete",
 *     "collection" =  "/admin/commerce/config/store/reactions"
 *   }
 * )
 */
class Reaction extends ConfigEntityBase implements ReactionInterface {

  /**
   * The commerce reaction ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The commerce reaction label.
   *
   * @var string
   */
  protected $label;

  /**
   * The event ID where this reaction should be ran.
   *
   * @var array
   */
  protected $event;

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * The conditions.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * The plugin collection that holds the commerce reaction plugin.
   *
   * @var \Drupal\commerce\CommerceSinglePluginCollection
   */
  protected $pluginCollection;

  /**
   * {@inheritdoc}
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * {@inheritdoc}
   */
  public function setEvent($event) {
    $this->event = $event;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->getPluginCollection()->get($this->plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId($plugin_id) {
    $this->plugin = $plugin_id;
    $this->configuration = [];
    $this->pluginCollection = NULL;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginConfiguration(array $configuration) {
    $this->configuration = $configuration;
    $this->pluginCollection = NULL;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'configuration' => $this->getPluginCollection(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions($context_name = NULL) {
    $plugin_manager = \Drupal::service('plugin.manager.commerce_condition');
    $conditions = [];
    foreach ($this->conditions as $context_id => $context) {
      foreach ($context['conditions'] as $condition) {
        $condition = $plugin_manager->createInstance($condition['plugin'], $condition['configuration']);
        if ($condition instanceof ParentEntityAwareInterface) {
          $condition->setParentEntity($this);
        }
        $conditions[$context_id][] = $condition;
      }
    }

    // Return all conditions.
    if (empty($context_name)) {
      return $conditions;
    }
    // Return only the specific conditions if they exists.
    elseif (!empty($conditions[$context_name])) {
      return $conditions[$context_name];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionsOperator($context_name) {
    return $this->conditions[$context_name]['operator'];
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionsOperator($context_name, $condition_operator) {
    $this->conditions[$context_name]['operator'] = $condition_operator;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(array $contexts) {
    $conditions = $this->getConditions();
    if (!$conditions) {
      // Commerce reactions without conditions always apply.
      return TRUE;
    }

    $results = [];
    foreach ($conditions as $context_name => $context) {
      if (empty($contexts[$context_name])) {
        // Don't apply if missing context.
        return FALSE;
      }
      else {
        $entity = $contexts[$context_name];

        // Some context could be an array, so we need to evaluate all items.
        if (!is_array($entity)) {
          $entity = [$entity];
        }

        $context_conditions = $context_results = [];
        foreach ($entity as $current_entity) {
          // Only create the new condition group if it isn't exists.
          if (empty($context_conditions[$context_name])) {
            $context_conditions[$context_name] = array_filter($conditions[$context_name], function ($condition) use ($current_entity) {
              /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $condition */
              return $condition->getEntityTypeId() == $current_entity->getEntityTypeId();
            });
            $group_conditions = new ConditionGroup($context_conditions[$context_name], $this->getConditionsOperator($context_name));
          }

          $context_results[] = $group_conditions->evaluate($current_entity);
        }

        $results[] = $this->reduceResult($context_results, 'OR');
      }
    }

    return $this->reduceResult($results, 'AND');
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value) {
    // Invoke the setters to clear related properties.
    if ($property_name == 'plugin') {
      $this->setPluginId($value);
    }
    elseif ($property_name == 'configuration') {
      $this->setPluginConfiguration($value);
    }
    else {
      parent::set($property_name, $value);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    $this->addDependency('module', \Drupal::service('commerce_reactions.event_manager')->getEvent($this->getEvent())['provider']);

    return $this;
  }

  /**
   * Gets the plugin collection that holds the commerce reaction plugin.
   *
   * Ensures the plugin collection is initialized before returning it.
   *
   * @return \Drupal\commerce\CommerceSinglePluginCollection
   *   The plugin collection.
   */
  protected function getPluginCollection() {
    if (!$this->pluginCollection) {
      $plugin_manager = \Drupal::service('plugin.manager.commerce_reaction');
      $this->pluginCollection = new CommerceSinglePluginCollection($plugin_manager, $this->plugin, $this->configuration, $this->id);
    }

    return $this->pluginCollection;
  }

  /**
   * Reduce an array of boolean values.
   *
   * @param array $results
   *   The values.
   * @param string $operator
   *   The operator to use.
   *
   * @return bool
   *   The reduced value.
   */
  private function reduceResult(array $results, $operator = 'AND') {
    return array_reduce(
      $results,
      function ($a, $b) use ($operator) {
        if ($operator == 'OR') {
          return ($a === NULL) ? $b : $a || $b;
        }
        elseif ($operator == 'AND') {
          return ($a === NULL) ? $b : $a && $b;
        }
      }
    );
  }

}
