<?php

namespace Drupal\commerce_reactions\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a commerce reaction annotation object.
 *
 * Plugin Namespace: Plugin\CommerceReaction.
 *
 * For a working example, see
 * \Drupal\commerce_reaction\Plugin\CommerceReaction\SendEmail
 *
 * @see commerce_reactions_reaction_info
 * @see \Drupal\commerce_reactions\Plugin\CommerceReactionInterface
 * @see \Drupal\commerce_reactions\Plugin\CommerceReactionManager
 * @see \Drupal\commerce_reactions\Plugin\CommerceReactionBase
 * @see plugin_api
 *
 * @Annotation
 */
class CommerceReaction extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the reaction plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The category under which the reaction should be listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

  /**
   * The reaction entity type IDs.
   *
   * Empty for all types.
   *
   * @var array
   */
  public $entity_types = [];

}
