<?php

namespace Drupal\commerce_reactions\Form;

use Drupal\commerce_reactions\CommerceReactionEventManagerInterface;
use Drupal\commerce_reactions\Plugin\CommerceReactionManager;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides form for commerce reaction instance forms.
 */
class ReactionForm extends EntityForm {

  /**
   * The commerce reaction event manager.
   *
   * @var \Drupal\commerce_reactions\CommerceReactionEventManagerInterface
   */
  protected $eventManager;

  /**
   * The commerce reaction plugin manager.
   *
   * @var \Drupal\commerce_reactions\Plugin\CommerceReactionManager
   */
  protected $pluginManager;

  /**
   * Constructs a new ReactionForm object.
   *
   * @param \Drupal\commerce_reactions\Plugin\CommerceReactionManager $plugin_manager
   *   The commerce reaction plugin manager.
   * @param \Drupal\commerce_reactions\CommerceReactionEventManagerInterface $event_manager
   *   The commerce reaction event manager.
   */
  public function __construct(CommerceReactionManager $plugin_manager, CommerceReactionEventManagerInterface $event_manager) {
    $this->eventManager = $event_manager;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.commerce_reaction'),
      $container->get('commerce_reactions.event_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\commerce_reactions\Entity\ReactionInterface $reaction */
    $reaction = $this->entity;

    $events = $this->eventManager->getEventOptions();

    // Use the first available event as the default value.
    if (!$reaction->getEvent()) {
      $group = reset($events);
      $event_ids = array_keys($group);
      $event = reset($event_ids);
      $reaction->setEvent($event);
    }
    // The form state will have a event value if #ajax was used.
    $event = $form_state->getValue('event', $reaction->getEvent());
    $event_definition = $this->eventManager->getEvent($event);

    $entity_types = $this->eventManager->getEventEntityContexts($event);
    $plugins = $this->pluginManager->getFilteredAndGroupedOptions($entity_types);

    // Use the first available plugin as the default value.
    if (!$reaction->getPluginId()) {
      $category = reset($plugins);
      $plugin_ids = array_keys($category);
      $plugin = reset($plugin_ids);
      $reaction->setPluginId($plugin);
    }
    // The form state will have a plugin value if #ajax was used.
    $plugin = $form_state->getValue('plugin', $reaction->getPluginId());
    // Pass the plugin configuration only if the plugin hasn't been changed via
    // #ajax.
    $plugin_configuration = $reaction->getPluginId() == $plugin ? $reaction->getPluginConfiguration() : [];

    $form_state->setTemporaryValue('entity_types', $entity_types);
    $form_state->setTemporaryValue('contexts', $event_definition['contexts']);

    $wrapper_id = Html::getUniqueId('commerce-reaction-form');
    $form['#tree'] = TRUE;
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';

    $form['#tree'] = TRUE;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $reaction->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $reaction->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_reactions\Entity\Reaction::load',
      ],
      '#disabled' => !$reaction->isNew(),
    ];
    $form['event'] = [
      '#type' => 'select',
      '#title' => $this->t('Event'),
      '#options' => $events,
      '#default_value' => $event,
      '#required' => TRUE,
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];
    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Plugin'),
      '#options' => $plugins,
      '#default_value' => $plugin,
      '#required' => TRUE,
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::ajaxRefresh',
        'wrapper' => $wrapper_id,
      ],
    ];
    $form['configuration'] = [
      '#type' => 'commerce_plugin_configuration',
      '#plugin_type' => 'commerce_reaction',
      '#plugin_id' => $plugin,
      '#default_value' => $plugin_configuration,
    ];

    foreach ($event_definition['contexts'] as $context_name => $context) {
      if (!empty($context['entity_id'])) {
        if (empty($form['conditions'])) {
          $form['conditions'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Conditions'),
          ];
        }
        $count = $context['return'] == 'array' ? 2 : 1;
        $default_values = $reaction->get('conditions');
        $form['conditions'][$context_name]['conditions'] = [
          '#type' => 'commerce_conditions',
          '#title' => $this->formatPlural(
            $count,
            "@context's conditions",
            "@context' conditions",
            ['@context' => $context['label']]
          ),
          '#parent_entity_type' => 'commerce_reaction',
          '#entity_types' => [$context['entity_id']],
          '#default_value' => $default_values[$context_name]['conditions'] ?? [],
        ];
        $form['conditions'][$context_name]['operator'] = [
          '#type' => 'radios',
          '#title' => $this->formatPlural(
            $count,
            "@context's condition operator",
            "@context' condition operator",
            ['@context' => $context['label']]
          ),
          '#title_display' => 'invisible',
          '#options' => [
            'AND' => $this->t('All conditions must pass'),
            'OR' => $this->t('Only one condition must pass'),
          ],
          '#default_value' => $default_values[$context_name]['operator'] ?? 'AND',
        ];
        if ($count == 2) {
          $form['conditions'][$context_name]['operator']['#description'] = $this->t('This conditions will be valid if any of the elements of the array passes the conditions.');
        }
      }
    }
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $reaction->status(),
    ];

    return $form;
  }

  /**
   * Ajax callback for the "Reaction form".
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\commerce_reactions\Entity\ReactionInterface $reaction */
    $reaction = $this->entity;
    $reaction->setPluginConfiguration($form_state->getValue(['configuration']));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();
    $this->messenger()->addMessage($this->t('Saved the %label commerce reaction.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.commerce_reaction.collection');
  }

}
