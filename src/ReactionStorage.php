<?php

namespace Drupal\commerce_reactions;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Storage handler for commerce reaction config entities.
 */
class ReactionStorage extends ConfigEntityStorage {

  /**
   * The Drupal kernel.
   *
   * @var \Drupal\Core\DrupalKernelInterface
   */
  protected $drupalKernel;


  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);

    $instance->drupalKernel = $container->get('kernel');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    $events_before = $this->getRegisteredEvents();
    $return = parent::save($entity);
    $this->updateRegisteredEvents($events_before);

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities) {
    parent::delete($entities);
    $events_before = $this->getRegisteredEvents();
    $this->updateRegisteredEvents($events_before);
  }

  /**
   * Returns a list of event names that are used by active commerce reactions.
   *
   * @return string[]
   *   The list of event names keyed by event name.
   */
  public function getRegisteredEvents() {
    $events = [];

    /** @var \Drupal\commerce_reactions\Entity\ReactionInterface $reaction */
    foreach ($this->loadByProperties(['status' => TRUE]) as $reaction) {
      $event_name = $reaction->getEvent();
      $events[$event_name] = $event_name;
    }

    return $events;
  }

  /**
   * Update the registered events and refresh the event subscribers.
   *
   * @param array $events_before
   *   The previous list of event names keyed by event name.
   */
  public function updateRegisteredEvents(array $events_before) {
    $events_after = $this->getRegisteredEvents();
    if ($events_before != $events_after) {
      $this->drupalKernel->invalidateContainer();
    }
  }

}
