<?php

/**
 * @file
 * Hooks related to Commerce Reactions module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the information provided by the commerce reaction annotation objects.
 *
 * @param array $definitions
 *   The array of webform handlers, keyed on the machine-readable element name.
 *
 * @see \Drupal\commerce_reactions\Annotation\CommerceReaction
 */
function hook_commerce_reactions_reaction_info_alter(array &$definitions) {

}

/**
 * Provide information about available commerce reaction event groups.
 *
 * @return array
 *   An associative array of available commerce reaction event groups. keyed on
 *   the machine-readable group name.
 */
function hook_commerce_reactions_events() {
  return [
    'my_module' => [
      'label' => t('My Module'),
      'events' => [
        MyModuleEvents::MY_MODULE_EVENT => [
          'id' => MyModuleEvents::MY_MODULE_EVENT,
          'class' => '\Drupal\my_module\Event\MyModuleEvent',
          'provider' => 'my_module',
          'contexts' => [
            'commerce_order' => [
              'label' => t('Order'),
              'getter' => 'getOrder',
              'return' => 'object',
              'entity_id' => 'commerce_order',
            ],
            'order_item' => [
              'label' => t('Order item'),
              'getter' => 'getOrderItem',
              'return' => 'object',
              'entity_id' => 'commerce_order_item',
            ],
          ],
        ],
      ],
    ],
  ];
}

/**
 * Alter the information provided by the commerce reaction event groups.
 *
 * @param array $events
 *   The array of commerce reaction event groups, keyed on the machine-readable
 *   group name.
 */
function hook_commerce_reactions_events_alter(array &$events) {

}

/**
 * @} End of "addtogroup hooks".
 */
